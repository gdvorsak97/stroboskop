# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://gdvorsak97@bitbucket.org/gdvorsak97/stroboskop.git
cd stroboskop
```

Naloga 6.2.3:
https://bitbucket.org/gdvorsak97/stroboskop/commits/693510e6e8f2471efd617c7953c1bb513f37eee3

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/gdvorsak97/stroboskop/commits/0746389042e1d79d0fab1acd69683fd6090a3ff4

Naloga 6.3.2:
https://bitbucket.org/gdvorsak97/stroboskop/commits/11741f7eecafb7c5812aafb39dbc55491367fb5a

Naloga 6.3.3:
https://bitbucket.org/gdvorsak97/stroboskop/commits/04422195cec0eeb78135d7d56baa0c5d53b6fa8e

Naloga 6.3.4:
https://bitbucket.org/gdvorsak97/stroboskop/commits/453f39ef2bfdff8f5011a5166f05e9ef56d0593b

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/gdvorsak97/stroboskop/commits/dfcc82d534de65573e8debdbea158a7b561f27ba

Naloga 6.4.2:
https://bitbucket.org/gdvorsak97/stroboskop/commits/0c2ae0642077c5b00c0600ffa3818a2f1f9f6c25

Naloga 6.4.3:
https://bitbucket.org/gdvorsak97/stroboskop/commits/2c1c5fb57f999604d80b09d8279c8edb571ccb60

Naloga 6.4.4:
https://bitbucket.org/gdvorsak97/stroboskop/commits/86eaa6c529fcb2df18643bb7479c8ecd7e0a7e62